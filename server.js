'use strict';
const express = require('express');
const app = express()
const bodyParser = require('body-parser');
const request = require('request');
const fs = require('fs');
const openload = require('openload-link');
const fileUpload = require('express-fileupload');

let rawdata = fs.readFileSync('data.json');
let student = JSON.parse(rawdata);

app.use(express.static('public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.set('view engine', 'ejs');
app.use(fileUpload({
    limits: { fileSize: 1024 * 1024 * 1024 },
}));

app.get('/', function (req, res) {
        if (student.type == "TV") {
            res.render('tv', {
                student: student
            });
        } else {
            res.render('movies', {
                student: student
            });
        }
});

app.get('/seasons/:id', function (req, res) {
        let seasonNumber = parseInt(req.params.id) + 1;
        let requestedSeason = student.seasons.find(function (season) {
            return season.name == `SEASON ${seasonNumber}`;
        });
        res.render('season', { season: requestedSeason });

});

app.get('/servers/', function (req, res) {
        let movies = student;
        res.render('servers', { movies: movies });
    

});

app.get('/video/', function (req, res) {
        let get_link = req.query.url;
        if (get_link.match(/openload/ig)) {
            openload(get_link).then(output_link => {
                res.setHeader('Content-Type', 'application/json');
                res.send(JSON.stringify({ link: output_link }));
            });
        } else {
            return "#";
        }
});

app.get('/upload', function (req, res) {
        res.render('upload');
});

app.post('/upload', function (req, res) {
        if (!req.files) {
            return res.status(400).send('No files were uploaded.');
        }
        let sampleFile = req.files.data;
        sampleFile.mv('data.json', function (err) {
            if (err)
                return res.status(500).send(err);
            res.send('File uploaded!');
        });
});


app.listen(3000, function () {
    console.log('Example app listening on port 3000!')
});

