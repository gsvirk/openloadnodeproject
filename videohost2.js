const puppeteer = require('puppeteer');

const getLink = async link => {
    return new Promise(async (resolve, reject) => {
        try {
            console.log(link);
            let id = await link.split('/')[link.split('/').length]
            const browser = await puppeteer.launch({ args: ['--no-sandbox', '--disable-setuid-sandbox'] })
            const page = await browser.newPage()
            await page.goto(link)
            await page.waitForSelector('video')
            // await page.waitForFunction(`$('div[style="display:none;"] > p:contains("${id}")')`)
            // let file = await page.evaluate(() => document.querySelectorAll('video > source')[1].innerText)

            await browser.close()
            //  console.log('https://space-cdn.kodrive.xyz/b32./' + file);
            resolve('https://space-cdn.kodrive.xyz/b32./' + file)
        } catch (err) {
            reject(err)
        }
    })
}
module.exports = getLink